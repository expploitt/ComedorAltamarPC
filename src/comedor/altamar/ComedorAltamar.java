/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comedor.altamar;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Calendar;
import java.util.HashMap;
import javax.swing.JOptionPane;

/**
 *
 * @author expploitt
 */
public class ComedorAltamar implements Runnable {

    private final int PORT = 2045;
    public static HashMap<String, Persona> alberguistas = new HashMap<>();
    public static HashMap<String, Comida> comidas = new HashMap<>();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Ficheros operaciones = new Ficheros();
        JVentana ventana = new JVentana();
        
        
      
        /*CARGAMOS LOS DATOS DE LOS FICHEROS*/
        try {
            operaciones.leerPersonas();
        } catch (Exception e) {
            ventana.error10();
        }
        try {
            operaciones.leerComidas();
            operaciones.leerComidasFut();
        } catch (Exception e) {
            ventana.error15();
        }

        /*CREAMOS EL HILO PARA LA ESCUCHA*/
        Runnable comedorAltamar = new ComedorAltamar();
        new Thread(comedorAltamar).start();

    }

    /**
     * Hilo de escucha de peticiones de clientes para apuntarse o borrarse en
     * las distintas comidas
     */
    @Override
    public void run() {

        try {

            ServerSocket socket = new ServerSocket(PORT);

            while (true) {
                System.out.println("comedor.altamar.ComedorAltamar.run()");
                Socket cliente = socket.accept();
                System.out.println("Conectado!");
                /*CREAMOS EL HILO*/
                Runnable thread = new Hilo(cliente);
                new Thread(thread).start();

            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
